# Final Projects

## Assignment

By groups, you will design, fabricate and document a proof of concept of a scientifically-based and frugal solution to solve a real-world problem you have identified.

* You will position your project along the [17 Sustainable Development Goals](https://sdgs.un.org/) listed by the United Nations.
* You will show scientific evidence with sources and references on which you build on your project.
* You will use at least one digital fabrication technique that is used in a fablab.
* You will explain the team dynamics and demonstrate how you worked as a team.

## Final projects documentation
*  
*  
*  


## Preparation for the final presentation

For the final presentation, you will prepare

* a graphical abstract illustrating your question and project (1920x1080px PNG image entitled "firstname.surname-slide.png", [examples here](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/final-projects/final-projects/)) with :
    * a photo/image illustrating your project
    * [a logo of the FabLab ULB](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/tree/main/vade-mecum/images)
    * a short and clear question that your project tries to answer
* a pdf presentation (20 slides/10min) to share your project with an interdisciplinary audience ("firstname.surname-presentation.pdf").
* prototypes, proof of concept and technical/scientific documents that illustrate your process and your final achievement.
* a printed QR code linking to your online documentation
